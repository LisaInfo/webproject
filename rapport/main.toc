\select@language {french}
\contentsline {chapter}{\numberline {1}D\IeC {\'e}marche}{1}
\contentsline {chapter}{\numberline {2}Choix et outils utilis\IeC {\'e}s}{3}
\contentsline {section}{\numberline {2.1}Django}{3}
\contentsline {subsection}{\numberline {2.1.1}Une solution adapt\IeC {\'e}e aux besoins}{3}
\contentsline {subsection}{\numberline {2.1.2}Installation et lancement de l'application}{3}
\contentsline {section}{\numberline {2.2}Architecture de l'application}{4}
\contentsline {subsection}{\numberline {2.2.1}Mod\IeC {\`e}les}{4}
\contentsline {subsection}{\numberline {2.2.2}Templates}{5}
\contentsline {subsection}{\numberline {2.2.3}Vues}{9}
\contentsline {section}{\numberline {2.3}Gestion de versions}{10}
\contentsline {chapter}{\numberline {3}Conclusion}{11}
\contentsline {section}{\numberline {3.1}Analyse des probl\IeC {\`e}mes rencontr\IeC {\'e}s}{11}
\contentsline {section}{\numberline {3.2}Perspectives de d\IeC {\'e}veloppement}{12}
\contentsline {chapter}{Sitographie}{13}
