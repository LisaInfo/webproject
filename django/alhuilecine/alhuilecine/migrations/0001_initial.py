# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Artiste',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('nom', models.CharField(max_length=100)),
                ('prenom', models.CharField(blank=True, max_length=100)),
                ('datenaissance', models.DateField(null=True, blank=True)),
                ('datemort', models.DateField(null=True, blank=True)),
                ('biographie', models.CharField(blank=True, max_length=1000)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Artisteavoirnationalite',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('artiste', models.ForeignKey(to='alhuilecine.Artiste')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AuthGroup',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(unique=True, max_length=80)),
            ],
            options={
                'db_table': 'auth_group',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AuthGroupPermissions',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('group', models.ForeignKey(to='alhuilecine.AuthGroup')),
            ],
            options={
                'db_table': 'auth_group_permissions',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AuthPermission',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
                ('codename', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'auth_permission',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AuthUser',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('password', models.CharField(max_length=128)),
                ('last_login', models.DateTimeField()),
                ('is_superuser', models.BooleanField()),
                ('username', models.CharField(unique=True, max_length=30)),
                ('first_name', models.CharField(max_length=30)),
                ('last_name', models.CharField(max_length=30)),
                ('email', models.CharField(max_length=75)),
                ('is_staff', models.BooleanField()),
                ('is_active', models.BooleanField()),
                ('date_joined', models.DateTimeField()),
            ],
            options={
                'db_table': 'auth_user',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AuthUserGroups',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('group', models.ForeignKey(to='alhuilecine.AuthGroup')),
                ('user', models.ForeignKey(to='alhuilecine.AuthUser')),
            ],
            options={
                'db_table': 'auth_user_groups',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AuthUserUserPermissions',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('permission', models.ForeignKey(to='alhuilecine.AuthPermission')),
                ('user', models.ForeignKey(to='alhuilecine.AuthUser')),
            ],
            options={
                'db_table': 'auth_user_user_permissions',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Avoirgenre',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Avoirpourreponse',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Changementstatut',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('datechangement', models.DateField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DjangoAdminLog',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('action_time', models.DateTimeField()),
                ('object_id', models.TextField(blank=True)),
                ('object_repr', models.CharField(max_length=200)),
                ('action_flag', models.PositiveSmallIntegerField()),
                ('change_message', models.TextField()),
            ],
            options={
                'db_table': 'django_admin_log',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DjangoContentType',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('app_label', models.CharField(max_length=100)),
                ('model', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'django_content_type',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DjangoMigrations',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('app', models.CharField(max_length=255)),
                ('name', models.CharField(max_length=255)),
                ('applied', models.DateTimeField()),
            ],
            options={
                'db_table': 'django_migrations',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DjangoSession',
            fields=[
                ('session_key', models.CharField(primary_key=True, serialize=False, max_length=40)),
                ('session_data', models.TextField()),
                ('expire_date', models.DateTimeField()),
            ],
            options={
                'db_table': 'django_session',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Etreacteur',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('artiste', models.ForeignKey(to='alhuilecine.Artiste')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Etrepersonnage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Etrerealisateur',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('artiste', models.ForeignKey(to='alhuilecine.Artiste')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Film',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('titre', models.CharField(max_length=100)),
                ('datesortie', models.DateField()),
                ('synopsis', models.CharField(blank=True, max_length=1000)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Filmavoirnationalite',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('film', models.ForeignKey(to='alhuilecine.Film')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Forum',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Forumconcernerfilm',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('film', models.ForeignKey(to='alhuilecine.Film')),
                ('forum', models.ForeignKey(to='alhuilecine.Forum')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Forumconcernergroupe',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('forum', models.ForeignKey(to='alhuilecine.Forum')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('nom', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Groupe',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('nom', models.CharField(max_length=100)),
                ('prive', models.BooleanField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Groupeconcernerartiste',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('artiste', models.ForeignKey(to='alhuilecine.Artiste')),
                ('groupe', models.ForeignKey(to='alhuilecine.Groupe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Groupeconcernerfilm',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('film', models.ForeignKey(to='alhuilecine.Film')),
                ('groupe', models.ForeignKey(to='alhuilecine.Groupe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Groupeconcernergenre',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('genre', models.ForeignKey(to='alhuilecine.Genre')),
                ('groupe', models.ForeignKey(to='alhuilecine.Groupe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Groupeconcernernationalite',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('groupe', models.ForeignKey(to='alhuilecine.Groupe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Groupeconcernerpersonnage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('groupe', models.ForeignKey(to='alhuilecine.Groupe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Incarnerpersonnage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('artiste', models.ForeignKey(to='alhuilecine.Artiste')),
                ('film', models.ForeignKey(to='alhuilecine.Film')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('texte', models.CharField(max_length=3000)),
                ('dateenvoi', models.DateField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Nationalite',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('nom', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('valeur', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Noter',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('film', models.ForeignKey(to='alhuilecine.Film')),
                ('note', models.ForeignKey(to='alhuilecine.Note')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Optionsondage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('texte', models.CharField(max_length=1000)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Personnage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('nom', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sondage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('question', models.CharField(max_length=1000)),
                ('dateproposition', models.DateField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sondageconcernergroupe',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('groupe', models.ForeignKey(to='alhuilecine.Groupe')),
                ('sondage', models.ForeignKey(to='alhuilecine.Sondage')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Statut',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('nom', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Utilisateur',
            fields=[
                ('pseudo', models.CharField(primary_key=True, serialize=False, max_length=20)),
                ('motdepasse', models.CharField(max_length=20)),
                ('statut', models.ForeignKey(to='alhuilecine.Statut')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Votesondage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('optionsondage', models.ForeignKey(to='alhuilecine.Optionsondage')),
                ('sondage', models.ForeignKey(to='alhuilecine.Sondage')),
                ('voteur', models.ForeignKey(to='alhuilecine.Utilisateur')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='sondage',
            name='proposeur',
            field=models.ForeignKey(to='alhuilecine.Utilisateur'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='optionsondage',
            name='sondage',
            field=models.ForeignKey(to='alhuilecine.Sondage'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='noter',
            name='utilisateur',
            field=models.ForeignKey(to='alhuilecine.Utilisateur'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='message',
            name='envoyeur',
            field=models.ForeignKey(to='alhuilecine.Utilisateur'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='message',
            name='forum',
            field=models.ForeignKey(to='alhuilecine.Forum'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='incarnerpersonnage',
            name='personnage',
            field=models.ForeignKey(to='alhuilecine.Personnage'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='groupeconcernerpersonnage',
            name='personnage',
            field=models.ForeignKey(to='alhuilecine.Personnage'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='groupeconcernernationalite',
            name='nationalite',
            field=models.ForeignKey(to='alhuilecine.Nationalite'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='forumconcernergroupe',
            name='groupe',
            field=models.ForeignKey(to='alhuilecine.Groupe'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='forum',
            name='moderateur',
            field=models.ForeignKey(to='alhuilecine.Utilisateur'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='filmavoirnationalite',
            name='nationalite',
            field=models.ForeignKey(to='alhuilecine.Nationalite'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='etrerealisateur',
            name='film',
            field=models.ForeignKey(to='alhuilecine.Film'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='etrepersonnage',
            name='film',
            field=models.ForeignKey(to='alhuilecine.Film'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='etrepersonnage',
            name='personnage',
            field=models.ForeignKey(to='alhuilecine.Personnage'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='etreacteur',
            name='film',
            field=models.ForeignKey(to='alhuilecine.Film'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='djangoadminlog',
            name='content_type',
            field=models.ForeignKey(to='alhuilecine.DjangoContentType', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='djangoadminlog',
            name='user',
            field=models.ForeignKey(to='alhuilecine.AuthUser'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='changementstatut',
            name='administrateur',
            field=models.ForeignKey(to='alhuilecine.Utilisateur', related_name='administrateur'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='changementstatut',
            name='ancienstatut',
            field=models.ForeignKey(to='alhuilecine.Statut', related_name='ancienstatut'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='changementstatut',
            name='nouveaustatut',
            field=models.ForeignKey(to='alhuilecine.Statut', related_name='nouveaustatut'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='changementstatut',
            name='utilisateur',
            field=models.ForeignKey(to='alhuilecine.Utilisateur'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='avoirpourreponse',
            name='messagefils',
            field=models.ForeignKey(to='alhuilecine.Message', related_name='messagefils'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='avoirpourreponse',
            name='messagepere',
            field=models.ForeignKey(to='alhuilecine.Message', related_name='messagepere'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='avoirgenre',
            name='film',
            field=models.ForeignKey(to='alhuilecine.Film'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='avoirgenre',
            name='genre',
            field=models.ForeignKey(to='alhuilecine.Genre'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='authpermission',
            name='content_type',
            field=models.ForeignKey(to='alhuilecine.DjangoContentType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='authgrouppermissions',
            name='permission',
            field=models.ForeignKey(to='alhuilecine.AuthPermission'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='artisteavoirnationalite',
            name='nationalite',
            field=models.ForeignKey(to='alhuilecine.Nationalite'),
            preserve_default=True,
        ),
    ]
