# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alhuilecine', '0002_auto_20150303_1934'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='authgrouppermissions',
            name='group',
        ),
        migrations.RemoveField(
            model_name='authgrouppermissions',
            name='permission',
        ),
        migrations.DeleteModel(
            name='AuthGroupPermissions',
        ),
        migrations.RemoveField(
            model_name='authpermission',
            name='content_type',
        ),
        migrations.RemoveField(
            model_name='authusergroups',
            name='group',
        ),
        migrations.DeleteModel(
            name='AuthGroup',
        ),
        migrations.RemoveField(
            model_name='authusergroups',
            name='user',
        ),
        migrations.DeleteModel(
            name='AuthUserGroups',
        ),
        migrations.RemoveField(
            model_name='authuseruserpermissions',
            name='permission',
        ),
        migrations.DeleteModel(
            name='AuthPermission',
        ),
        migrations.RemoveField(
            model_name='authuseruserpermissions',
            name='user',
        ),
        migrations.DeleteModel(
            name='AuthUserUserPermissions',
        ),
        migrations.RemoveField(
            model_name='djangoadminlog',
            name='content_type',
        ),
        migrations.RemoveField(
            model_name='djangoadminlog',
            name='user',
        ),
        migrations.DeleteModel(
            name='AuthUser',
        ),
        migrations.DeleteModel(
            name='DjangoAdminLog',
        ),
        migrations.DeleteModel(
            name='DjangoContentType',
        ),
        migrations.DeleteModel(
            name='DjangoMigrations',
        ),
        migrations.DeleteModel(
            name='DjangoSession',
        ),
    ]
