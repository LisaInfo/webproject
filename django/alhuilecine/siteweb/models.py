#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

class Nationalite(models.Model):
    nom = models.CharField(max_length = 100, verbose_name = u"Intitulé")
    
    def __str__(self):
        return self.nom
        
    class Meta:
        ordering = ['nom']

class Artiste(models.Model):
    nom = models.CharField(max_length = 100, verbose_name = u"Nom")
    prenom = models.CharField(max_length = 100, blank = True, verbose_name = u"Prénom")
    datenaissance = models.DateField(blank = True, null = True, verbose_name = u"Date de naissance (AAAA-MM-JJ)")
    datemort = models.DateField(blank = True, null = True, verbose_name = u"Date de mort (AAAA-MM-JJ)")
    biographie = models.TextField(max_length = 3000, blank = True, verbose_name = u"Biographie")
    nationalites = models.ManyToManyField(Nationalite, verbose_name = u"Nationalités", help_text = "<br/>", blank = True)

    def __str__(self):
        return self.prenom + " " + self.nom
        
    class Meta:
        ordering = ['nom']
    
class Personnage(models.Model):
    nom = models.CharField(max_length = 100, verbose_name = "Nom")
    
    def __str__(self):
        return self.nom
        
    class Meta:
        ordering = ['nom']
    
class Genre(models.Model):
    nom = models.CharField(max_length = 100, verbose_name = u"Intitulé")
    
    def __str__(self):
        return self.nom
        
    class Meta:
        ordering = ['nom']
        
class Forum(models.Model):
    moderateur = models.ForeignKey(User, verbose_name = u"Modérateur")
    
    def __str__(self):
        return u"Forum " + str(self.id) + u" modere par " + str(self.moderateur)

class Note(models.Model):
    valeur = models.IntegerField(verbose_name = u"Note")
   
    def __str__(self):
        return u"Note de " + str(self.valeur)
    
class Film(models.Model):
    titre = models.CharField(max_length = 100, verbose_name = u"Film")
    datesortie = models.DateField(verbose_name = u"Date de sortie (AAAA-MM-JJ)")
    synopsis = models.TextField(max_length = 3000, blank = True, verbose_name = u"Synopsis")
    acteurs = models.ManyToManyField(Artiste, verbose_name = u"Acteurs", help_text = "<br/>", related_name = 'acteurs', blank = True)
    realisateurs = models.ManyToManyField(Artiste, verbose_name = u"Réalisateurs", help_text = "<br/>", related_name = 'realisateurs', blank = True)
    personnages = models.ManyToManyField(Personnage, through='Incarnerpersonnage', verbose_name = u"Personnages", help_text = "<br/>", blank = True)
    genres = models.ManyToManyField(Genre, verbose_name = u"Genres", help_text = "<br/>", blank = True)
    nationalites = models.ManyToManyField(Nationalite, verbose_name = u"Nationalités", help_text = "<br/>", blank = True)
    forum = models.OneToOneField(Forum, verbose_name = u"Forum", help_text = "<br/>")
	
    def __str__(self):
        return self.titre + " (" + str(self.datesortie.year) + ")"
        
    class Meta:
        ordering = ['titre']
    
class Incarnerpersonnage(models.Model):
    personnage = models.ForeignKey(Personnage)
    film = models.ForeignKey(Film)
    artiste = models.ForeignKey(Artiste)
    
    def __str__(self):
        return str(self.artiste) + u" incarne " + str(self.personnage) + " dans " + str(self.film)

class Message(models.Model):
    texte = models.TextField(max_length = 3000, verbose_name = u"Message")
    dateenvoi = models.DateTimeField(default=datetime.now)
    forum = models.ForeignKey(Forum)
    envoyeur = models.ForeignKey(User)
    enreponsea = models.ForeignKey('self', blank = True, null = True)
    
    def __str__(self):
        return u"Message du " + str(self.dateenvoi) + u" par " + self.envoyeur.username + u" sur le forum \"" + str(self.forum) + u"\" : \"" + self.texte + u"\""
        
    class Meta:
        ordering = ['-dateenvoi']

class Groupe(models.Model):
    nom = models.CharField(max_length = 100, verbose_name = u"Nom")
    prive = models.BooleanField(default = False)
    forum = models.OneToOneField(Forum, verbose_name = u"Forum", help_text = "<br/>")
    artistes = models.ManyToManyField(Artiste, verbose_name = u"Artistes", help_text = "<br/>", blank = True)
    films = models.ManyToManyField(Film, verbose_name = u"Films", help_text = "<br/>", blank = True)
    personnages = models.ManyToManyField(Personnage, verbose_name = u"Personnages", help_text = "<br/>", blank = True)
    genres = models.ManyToManyField(Genre, verbose_name = u"Genres", help_text = "<br/>", blank = True)
    nationalites = models.ManyToManyField(Nationalite, verbose_name = u"Nationalités", help_text = "<br/>", blank = True)
    
    def __str__(self):
        return u"Groupe " + (u"prive" if self.prive else u"public") + " \"" + self.nom + "\""

class Noter(models.Model):
    utilisateur = models.ForeignKey(User)
    film = models.ForeignKey(Film)
    note = models.ForeignKey(Note)
    
    def __str__(self):
        return str(self.utilisateur) + u" a note " + str(self.film) + u" : \"" + str(self.note) + u"\""

class Sondage(models.Model):
    question = models.CharField(max_length = 1000, verbose_name = u"Question")
    dateproposition = models.DateField()
    proposeur = models.ForeignKey(User)
    
    def __str__(self):
        return u"Sondage propose par " + str(self.proposeur) + u" le " + str(self.dateproposition) + u" : \"" + self.question + u"\""

class Optionsondage(models.Model):
    sondage = models.ForeignKey(Sondage, verbose_name = u"Option")
    texte = models.CharField(max_length = 1000)
    
    def __str__(self):
        return u"Option \"" + self.texte + u"\" dans le sondage " + str(self.sondage)

class Sondageconcernergroupe(models.Model):
    sondage = models.ForeignKey(Sondage)
    groupe = models.ForeignKey(Groupe)

    def __str__(self):
        return str(self.sondage) + u" concerne " + str(self.groupe)

class Votesondage(models.Model):
    voteur = models.ForeignKey(User)
    sondage = models.ForeignKey(Sondage)
    optionsondage = models.ForeignKey(Optionsondage)
    
    def __str__(self):
        return str(self.voteur) + u" a vote l'option " + str(self.optionsondage)
