#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib import admin
from siteweb.models import *

# Register your models here.
admin.site.register(Artiste)
admin.site.register(Nationalite)
admin.site.register(Film)
admin.site.register(Genre)
admin.site.register(Forum)
admin.site.register(Message)
admin.site.register(Personnage)
admin.site.register(Groupe)
admin.site.register(Incarnerpersonnage)
admin.site.register(Note)
admin.site.register(Noter)
admin.site.register(Sondage)
admin.site.register(Optionsondage)
admin.site.register(Sondageconcernergroupe)
admin.site.register(Votesondage)
