import datetime
from haystack import indexes
from siteweb.models import *

class FilmIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document = True, use_template = True)
    rendered = indexes.CharField(use_template = True, indexed = False)

    def get_model(self):
        return Film

class GenreIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document = True, use_template = True)
    rendered = indexes.CharField(use_template = True, indexed = False)

    def get_model(self):
        return Genre

class NationaliteIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document = True, use_template = True)
    rendered = indexes.CharField(use_template = True, indexed = False)

    def get_model(self):
        return Nationalite
        
class ArtisteIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document = True, use_template = True)
    rendered = indexes.CharField(use_template = True, indexed = False)

    def get_model(self):
        return Artiste

class PersonnageIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document = True, use_template = True)
    rendered = indexes.CharField(use_template = True, indexed = False)

    def get_model(self):
        return Personnage

