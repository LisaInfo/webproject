#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from siteweb import views
from django.contrib.auth.forms import UserCreationForm
from django.views import generic
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

urlpatterns = patterns('',
    url(r'^$', views.index, name = 'index'),
    (r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'siteweb/login.html'}),
    (r'^accounts/logout/$', 'django.contrib.auth.views.logout_then_login', {}),
    url(r'^accounts/register/$', (generic.CreateView.as_view(model = User, get_success_url = lambda: reverse('django.contrib.auth.views.logout_then_login'), form_class=UserCreationForm, template_name="siteweb/register.html")), name='accounts/register'),
    url(r'^films/$', views.FilmListView.as_view(), name = 'films'),
    url(r'^film/(?P<pk>\d+)/$', views.FilmDetailView.as_view(), name = 'film'),
    url(r'^ajouter/film/$', views.FilmCreateView.as_view(), name = 'ajouter/film'),
    url(r'^mettreajour/film/(?P<pk>\d+)/$', views.FilmUpdateView.as_view(), name = 'mettreajour/film'),
    url(r'^supprimer/film/(?P<pk>\d+)/$', views.FilmDeleteView.as_view(), name = 'supprimer/film'),
    url(r'^ajouter/message/(?P<forumId>\d+)/(?P<messageId>\d+)/$', views.MessageCreateView.as_view(), name = 'ajouter/message'),
    url(r'^ajouter/message/(?P<forumId>\d+)/$', views.MessageCreateView.as_view(), name = 'ajouter/message'),
    url(r'^artistes/$', views.ArtisteListView.as_view(), name = 'artistes'),
    url(r'^artiste/(?P<pk>\d+)/$', views.ArtisteDetailView.as_view(), name = 'artiste'),
    url(r'^ajouter/artiste/$', views.ArtisteCreateView.as_view(), name = 'ajouter/artiste'),
    url(r'^mettreajour/artiste/(?P<pk>\d+)/$', views.ArtisteUpdateView.as_view(), name = 'mettreajour/artiste'),
    url(r'^supprimer/artiste/(?P<pk>\d+)/$', views.ArtisteDeleteView.as_view(), name = 'supprimer/artiste'),
    url(r'^genres/$', views.GenreListView.as_view(), name = 'genres'),
    url(r'^genre/(?P<pk>\d+)/$', views.GenreDetailView.as_view(), name = 'genre'),
    url(r'^ajouter/genre/$', views.GenreCreateView.as_view(), name = 'ajouter/genre'),
    url(r'^mettreajour/genre/(?P<pk>\d+)/$', views.GenreUpdateView.as_view(), name = 'mettreajour/genre'),
    url(r'^supprimer/genre/(?P<pk>\d+)/$', views.GenreDeleteView.as_view(), name = 'supprimer/genre'),
    url(r'^nationalites/$', views.NationaliteListView.as_view(), name = 'nationalites'),
    url(r'^nationalite/(?P<pk>\d+)/$', views.NationaliteDetailView.as_view(), name = 'nationalite'),
    url(r'^ajouter/nationalite/$', views.NationaliteCreateView.as_view(), name = 'ajouter/nationalite'),
    url(r'^mettreajour/nationalite/(?P<pk>\d+)/$', views.NationaliteUpdateView.as_view(), name = 'mettreajour/nationalite'),
    url(r'^supprimer/nationalite/(?P<pk>\d+)/$', views.NationaliteDeleteView.as_view(), name = 'supprimer/nationalite'),
    url(r'^personnages/$', views.PersonnageListView.as_view(), name = 'personnages'),
    url(r'^personnage/(?P<pk>\d+)/$', views.PersonnageDetailView.as_view(), name = 'personnage'),
    url(r'^ajouter/personnage/$', views.PersonnageCreateView.as_view(), name = 'ajouter/personnage'),
    url(r'^mettreajour/personnage/(?P<pk>\d+)/$', views.PersonnageUpdateView.as_view(), name = 'mettreajour/personnage'),
    url(r'^supprimer/personnage/(?P<pk>\d+)/$', views.PersonnageDeleteView.as_view(), name = 'supprimer/personnage'),
    url(r'^incarnerpersonnages/$', views.IncarnerpersonnageListView.as_view(), name = 'incarnerpersonnages'),
    url(r'^ajouter/incarnerpersonnage/$', views.IncarnerpersonnageCreateView.as_view(), name = 'ajouter/incarnerpersonnage'),
    url(r'^mettreajour/incarnerpersonnage/(?P<pk>\d+)/$', views.IncarnerpersonnageUpdateView.as_view(), name = 'mettreajour/incarnerpersonnage'),
    url(r'^supprimer/incarnerpersonnage/(?P<pk>\d+)/$', views.IncarnerpersonnageDeleteView.as_view(), name = 'supprimer/incarnerpersonnage'),
    url(r'^ajouter/noter/(?P<filmId>\d+)/$', views.NoterCreateView.as_view(), name = 'ajouter/noter'),
    url(r'^mettreajour/noter/(?P<pk>\d+)/$', views.NoterUpdateView.as_view(), name = 'mettreajour/noter'),
    url(r'^recherche/', include('haystack.urls')),
)
