from django.template.defaulttags import register

@register.filter
def get_item(dico,key):
    return dico.get(key)
