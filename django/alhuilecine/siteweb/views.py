#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.core.urlresolvers import reverse
from django.views import generic
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from siteweb.models import *
from django.db.models import Avg

# Ajout récursif de messages avec leur indentation
def ajoutMessages(messages, liste, indentation, messageCourant):
    for message in messages:
        if not message.enreponsea or (messageCourant and message.enreponsea.id == messageCourant.id):
            liste.append({'message': message, 'indentation': indentation})
            ajoutMessages(message.message_set.order_by('dateenvoi'), liste, indentation + 4, message)

def calculerMoyenne(noter):
    s = 0
    n = 0
    for note in noter:
        s += note.note.valeur
        n += 1
    if n == 0:
        return None
    else:
        return s / n
    
# Index page
def index(request):
    return render(request, 'siteweb/index.html', {})

# Film list view
class FilmListView(generic.ListView):
    template_name = 'siteweb/films.html'
    context_object_name = 'films'

    def get_queryset(self):
        return Film.objects.order_by('titre')
        
    def get_context_data(self, **kwargs):
        context = super(FilmListView, self).get_context_data(**kwargs)
        context['avg'] = {}
        for film in self.object_list:
            context['avg'][film.id] = calculerMoyenne(film.noter_set.all())
        return context

# Film detail view
class FilmDetailView(generic.DetailView):
    model = Film
    template_name = 'siteweb/film.html'
    
    def get_context_data(self, **kwargs):
        context = super(FilmDetailView, self).get_context_data(**kwargs)
        context['forummessagesindentes'] = []
        ajoutMessages(self.object.forum.message_set.all(), context['forummessagesindentes'], 0, None)
        context['avg'] = calculerMoyenne(self.object.noter_set.all())
        context['noteDejaAjoutee'] = Noter.objects.filter(utilisateur = self.request.user, film = self.object)
        return context

# Film create view
class FilmCreateView(generic.CreateView):
    model = Film
    template_name = 'siteweb/ajouter/film.html'
    fields = ['titre', 'datesortie', 'genres', 'nationalites', 'acteurs', 'realisateurs', 'synopsis']
    
    def get_success_url(self):
        return reverse("film", kwargs = {'pk': self.object.id})
        
    # When the form validates, we create the forum
    def form_valid(self, form):
        forum = Forum.objects.create(moderateur = self.request.user)
        form.instance.forum = forum
        form.instance.save()
        return super(FilmCreateView, self).form_valid(form)
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(FilmCreateView, self).dispatch(*args, **kwargs)
        
# Film update view
class FilmUpdateView(generic.UpdateView):
    model = Film
    template_name = 'siteweb/mettreajour/film.html'
    fields = ['titre', 'datesortie', 'genres', 'nationalites', 'acteurs', 'realisateurs', 'synopsis']
    
    def get_success_url(self):
        return reverse("film", kwargs = {'pk': self.object.id})
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(FilmUpdateView, self).dispatch(*args, **kwargs)
        
# Film delete view
class FilmDeleteView(generic.DeleteView):
    model = Film
    template_name = 'siteweb/supprimer/film.html'
    
    def get_success_url(self):
        return reverse("films")
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(FilmDeleteView, self).dispatch(*args, **kwargs)


# Message create view
class MessageCreateView(generic.CreateView):
    model = Message
    template_name = 'siteweb/ajouter/message.html'
    fields = ['texte']
    
    def get_success_url(self):
        return reverse("film", kwargs = {'pk': self.object.forum.film.id})
        
    # When the form validates, we create the forum
    def form_valid(self, form):
        form.instance.envoyeur = self.request.user
        form.instance.forum = Forum.objects.get(pk = self.kwargs['forumId'])
        if 'messageId' in self.kwargs.keys():
            form.instance.enreponsea = Message.objects.get(pk = self.kwargs['messageId']) 
        form.instance.save()
        return super(MessageCreateView, self).form_valid(form)
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MessageCreateView, self).dispatch(*args, **kwargs)


# Artiste list view
class ArtisteListView(generic.ListView):
    template_name = 'siteweb/artistes.html'
    context_object_name = 'artistes'

    def get_queryset(self):
        return Artiste.objects.order_by('nom')
        
# Artiste detail view
class ArtisteDetailView(generic.DetailView):
    model = Artiste
    template_name = 'siteweb/artiste.html'
    
# Artiste create view
class ArtisteCreateView(generic.CreateView):
    model = Artiste
    template_name = 'siteweb/ajouter/artiste.html'
    fields = ['nom', 'prenom', 'datenaissance', 'datemort', 'nationalites', 'biographie']
    
    def get_success_url(self):
        return reverse("artiste", kwargs = {'pk': self.object.id})
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ArtisteCreateView, self).dispatch(*args, **kwargs)
        
# Artiste update view
class ArtisteUpdateView(generic.UpdateView):
    model = Artiste
    template_name = 'siteweb/mettreajour/artiste.html'
    fields = ['nom', 'prenom', 'datenaissance', 'datemort', 'nationalites', 'biographie']
    
    def get_success_url(self):
        return reverse("artiste", kwargs = {'pk': self.object.id})
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ArtisteUpdateView, self).dispatch(*args, **kwargs)
        
# Artiste delete view
class ArtisteDeleteView(generic.DeleteView):
    model = Artiste
    template_name = 'siteweb/supprimer/artiste.html'
    
    def get_success_url(self):
        return reverse("artistes")
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ArtisteDeleteView, self).dispatch(*args, **kwargs)
        
# Genre list view
class GenreListView(generic.ListView):
    template_name = 'siteweb/genres.html'
    context_object_name = 'genres'

    def get_queryset(self):
        return Genre.objects.order_by('nom')
        
# Genre detail view
class GenreDetailView(generic.DetailView):
    model = Genre
    template_name = 'siteweb/genre.html'
    
# Genre create view
class GenreCreateView(generic.CreateView):
    model = Genre
    template_name = 'siteweb/ajouter/genre.html'
    fields = ['nom']
    
    def get_success_url(self):
        return reverse("genre", kwargs = {'pk': self.object.id})
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(GenreCreateView, self).dispatch(*args, **kwargs)
        
# Genre update view
class GenreUpdateView(generic.UpdateView):
    model = Genre
    template_name = 'siteweb/mettreajour/genre.html'
    fields = ['nom']
    
    def get_success_url(self):
        return reverse("genre", kwargs = {'pk': self.object.id})
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(GenreUpdateView, self).dispatch(*args, **kwargs)
        
# Genre delete view
class GenreDeleteView(generic.DeleteView):
    model = Genre
    template_name = 'siteweb/supprimer/genre.html'
    
    def get_success_url(self):
        return reverse("genres")
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(GenreDeleteView, self).dispatch(*args, **kwargs)
        
# Nationalite list view
class NationaliteListView(generic.ListView):
    template_name = 'siteweb/nationalites.html'
    context_object_name = 'nationalites'

    def get_queryset(self):
        return Nationalite.objects.order_by('nom')
        
# Nationalite detail view
class NationaliteDetailView(generic.DetailView):
    model = Nationalite
    template_name = 'siteweb/nationalite.html'
    
# Nationalite create view
class NationaliteCreateView(generic.CreateView):
    model = Nationalite
    template_name = 'siteweb/ajouter/nationalite.html'
    fields = ['nom']
    
    def get_success_url(self):
        return reverse("nationalite", kwargs = {'pk': self.object.id})
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(NationaliteCreateView, self).dispatch(*args, **kwargs)
        
# Nationalite update view
class NationaliteUpdateView(generic.UpdateView):
    model = Nationalite
    template_name = 'siteweb/mettreajour/nationalite.html'
    fields = ['nom']
    
    def get_success_url(self):
        return reverse("nationalite", kwargs = {'pk': self.object.id})
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(NationaliteUpdateView, self).dispatch(*args, **kwargs)
        
# Nationalite delete view
class NationaliteDeleteView(generic.DeleteView):
    model = Nationalite
    template_name = 'siteweb/supprimer/nationalite.html'
    
    def get_success_url(self):
        return reverse("nationalites")
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(NationaliteDeleteView, self).dispatch(*args, **kwargs)
        
# Personnage list view
class PersonnageListView(generic.ListView):
    template_name = 'siteweb/personnages.html'
    context_object_name = 'personnages'

    def get_queryset(self):
        return Personnage.objects.order_by('nom')
        
# Personnage detail view
class PersonnageDetailView(generic.DetailView):
    model = Personnage
    template_name = 'siteweb/personnage.html'
    
# Personnage create view
class PersonnageCreateView(generic.CreateView):
    model = Personnage
    template_name = 'siteweb/ajouter/personnage.html'
    fields = ['nom']
    
    def get_success_url(self):
        return reverse("personnage", kwargs = {'pk': self.object.id})
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PersonnageCreateView, self).dispatch(*args, **kwargs)
        
# Personnage update view
class PersonnageUpdateView(generic.UpdateView):
    model = Personnage
    template_name = 'siteweb/mettreajour/personnage.html'
    fields = ['nom']
    
    def get_success_url(self):
        return reverse("personnage", kwargs = {'pk': self.object.id})
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PersonnageUpdateView, self).dispatch(*args, **kwargs)
        
# Personnage delete view
class PersonnageDeleteView(generic.DeleteView):
    model = Personnage
    template_name = 'siteweb/supprimer/personnage.html'
    
    def get_success_url(self):
        return reverse("personnages")
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PersonnageDeleteView, self).dispatch(*args, **kwargs)

# Incarnerpersonnage list view
class IncarnerpersonnageListView(generic.ListView):
    template_name = 'siteweb/incarnerpersonnages.html'
    context_object_name = 'incarnerpersonnages'

    def get_queryset(self):
        return Incarnerpersonnage.objects.order_by('personnage')
    
# Incarnerpersonnage create view
class IncarnerpersonnageCreateView(generic.CreateView):
    model = Incarnerpersonnage
    template_name = 'siteweb/ajouter/incarnerpersonnage.html'
    fields = ['personnage', 'artiste', 'film']
    
    def get_success_url(self):
        return reverse("incarnerpersonnages")
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(IncarnerpersonnageCreateView, self).dispatch(*args, **kwargs)
                
# Incarnerpersonnage update view
class IncarnerpersonnageUpdateView(generic.UpdateView):
    model = Incarnerpersonnage
    template_name = 'siteweb/mettreajour/incarnerpersonnage.html'
    fields = ['personnage', 'artiste', 'film']
    
    def get_success_url(self):
        return reverse("incarnerpersonnages")
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(IncarnerpersonnageUpdateView, self).dispatch(*args, **kwargs)
        
# Incarnerpersonnage delete view
class IncarnerpersonnageDeleteView(generic.DeleteView):
    model = Incarnerpersonnage
    template_name = 'siteweb/supprimer/incarnerpersonnage.html'
    
    def get_success_url(self):
        return reverse("incarnerpersonnages")
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(IncarnerpersonnageDeleteView, self).dispatch(*args, **kwargs)
        
# Noter create view
class NoterCreateView(generic.CreateView):
    model = Noter
    template_name = 'siteweb/ajouter/noter.html'
    fields = ['note']
    
    def get_context_data(self, **kwargs):
        context = super(NoterCreateView, self).get_context_data(**kwargs)
        context['titre'] = Film.objects.get(pk = self.kwargs['filmId'])
        return context
    
    def get_success_url(self):
        return reverse("film",kwargs = {'pk': self.object.film.id})

    # When the form validates, we create the noter
    def form_valid(self, form):
        form.instance.utilisateur = self.request.user
        form.instance.film = Film.objects.get(pk = self.kwargs['filmId'])
        form.instance.save()
        return super(NoterCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(NoterCreateView, self).dispatch(*args, **kwargs)
        
# Noter update view
class NoterUpdateView(generic.UpdateView):
    model = Noter
    template_name = 'siteweb/mettreajour/noter.html'
    fields = ['note']
    
    def get_success_url(self):
        return reverse("film",kwargs = {'pk': self.object.film.id})
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(NoterUpdateView, self).dispatch(*args, **kwargs)
