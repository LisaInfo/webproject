# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('siteweb', '0009_auto_20150304_0512'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='enreponsea',
            field=models.ForeignKey(blank=True, to='siteweb.Message', null=True),
            preserve_default=True,
        ),
    ]
