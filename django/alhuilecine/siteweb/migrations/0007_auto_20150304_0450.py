# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('siteweb', '0006_auto_20150304_0449'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='texte',
            field=models.TextField(max_length=3000, verbose_name='Messae'),
            preserve_default=True,
        ),
    ]
