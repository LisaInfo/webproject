# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('siteweb', '0010_auto_20150304_0516'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='message',
            options={'ordering': ['dateenvoi']},
        ),
    ]
