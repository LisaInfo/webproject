# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Artiste',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('nom', models.CharField(verbose_name='Nom', max_length=100)),
                ('prenom', models.CharField(verbose_name='Prénom', max_length=100, blank=True)),
                ('datenaissance', models.DateField(verbose_name='Date de naissance (AAAA-MM-JJ)', null=True, blank=True)),
                ('datemort', models.DateField(verbose_name='Date de mort (AAAA-MM-JJ)', null=True, blank=True)),
                ('biographie', models.TextField(verbose_name='Biographie', max_length=3000, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Avoirpourreponse',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Film',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('titre', models.CharField(verbose_name='Film', max_length=100)),
                ('datesortie', models.DateField(verbose_name='Date de sortie (AAAA-MM-JJ)')),
                ('synopsis', models.TextField(verbose_name='Synopsis', max_length=3000, blank=True)),
                ('acteurs', models.ManyToManyField(help_text='<br/>', related_name='acteurs', verbose_name='Acteurs', to='siteweb.Artiste')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Forum',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('moderateur', models.ForeignKey(verbose_name='Modérateur', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Forumconcernerfilm',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('film', models.ForeignKey(to='siteweb.Film')),
                ('forum', models.ForeignKey(to='siteweb.Forum')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Forumconcernergroupe',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('forum', models.ForeignKey(to='siteweb.Forum')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('nom', models.CharField(verbose_name='Intitulé', max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Groupe',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('nom', models.CharField(verbose_name='Nom', max_length=100)),
                ('prive', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Groupeconcernerartiste',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('artiste', models.ForeignKey(to='siteweb.Artiste')),
                ('groupe', models.ForeignKey(to='siteweb.Groupe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Groupeconcernerfilm',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('film', models.ForeignKey(to='siteweb.Film')),
                ('groupe', models.ForeignKey(to='siteweb.Groupe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Groupeconcernergenre',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('genre', models.ForeignKey(to='siteweb.Genre')),
                ('groupe', models.ForeignKey(to='siteweb.Groupe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Groupeconcernernationalite',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('groupe', models.ForeignKey(to='siteweb.Groupe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Groupeconcernerpersonnage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('groupe', models.ForeignKey(to='siteweb.Groupe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Incarnerpersonnage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('artiste', models.ForeignKey(to='siteweb.Artiste')),
                ('film', models.ForeignKey(to='siteweb.Film')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('texte', models.TextField(verbose_name='Message', max_length=3000)),
                ('dateenvoi', models.DateField()),
                ('envoyeur', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('forum', models.ForeignKey(to='siteweb.Forum')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Nationalite',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('nom', models.CharField(verbose_name='Intitulé', max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('valeur', models.IntegerField(verbose_name='Note')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Noter',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('film', models.ForeignKey(to='siteweb.Film')),
                ('note', models.ForeignKey(to='siteweb.Note')),
                ('utilisateur', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Optionsondage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('texte', models.CharField(max_length=1000)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Personnage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('nom', models.CharField(verbose_name='Nom', max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sondage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('question', models.CharField(verbose_name='Question', max_length=1000)),
                ('dateproposition', models.DateField()),
                ('proposeur', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sondageconcernergroupe',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('groupe', models.ForeignKey(to='siteweb.Groupe')),
                ('sondage', models.ForeignKey(to='siteweb.Sondage')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Votesondage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('optionsondage', models.ForeignKey(to='siteweb.Optionsondage')),
                ('sondage', models.ForeignKey(to='siteweb.Sondage')),
                ('voteur', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='optionsondage',
            name='sondage',
            field=models.ForeignKey(verbose_name='Option', to='siteweb.Sondage'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='incarnerpersonnage',
            name='personnage',
            field=models.ForeignKey(to='siteweb.Personnage'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='groupeconcernerpersonnage',
            name='personnage',
            field=models.ForeignKey(to='siteweb.Personnage'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='groupeconcernernationalite',
            name='nationalite',
            field=models.ForeignKey(to='siteweb.Nationalite'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='forumconcernergroupe',
            name='groupe',
            field=models.ForeignKey(to='siteweb.Groupe'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='film',
            name='genres',
            field=models.ManyToManyField(help_text='<br/>', to='siteweb.Genre', verbose_name='Genres'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='film',
            name='nationalites',
            field=models.ManyToManyField(help_text='<br/>', to='siteweb.Nationalite', verbose_name='Nationalités'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='film',
            name='personnages',
            field=models.ManyToManyField(help_text='<br/>', to='siteweb.Personnage', verbose_name='Personnages', through='siteweb.Incarnerpersonnage'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='film',
            name='realisateurs',
            field=models.ManyToManyField(help_text='<br/>', related_name='realisateurs', verbose_name='Réalisateurs', to='siteweb.Artiste'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='avoirpourreponse',
            name='messagefils',
            field=models.ForeignKey(related_name='messagefils', to='siteweb.Message'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='avoirpourreponse',
            name='messagepere',
            field=models.ForeignKey(related_name='messagepere', to='siteweb.Message'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='artiste',
            name='nationalites',
            field=models.ManyToManyField(help_text='<br/>', to='siteweb.Nationalite', verbose_name='Nationalités'),
            preserve_default=True,
        ),
    ]
