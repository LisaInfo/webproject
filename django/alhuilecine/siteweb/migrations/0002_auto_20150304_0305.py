# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('siteweb', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artiste',
            name='nationalites',
            field=models.ManyToManyField(verbose_name='Nationalités', to='siteweb.Nationalite', blank=True, help_text='<br/>'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='film',
            name='acteurs',
            field=models.ManyToManyField(verbose_name='Acteurs', to='siteweb.Artiste', blank=True, help_text='<br/>', related_name='acteurs'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='film',
            name='genres',
            field=models.ManyToManyField(verbose_name='Genres', to='siteweb.Genre', blank=True, help_text='<br/>'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='film',
            name='nationalites',
            field=models.ManyToManyField(verbose_name='Nationalités', to='siteweb.Nationalite', blank=True, help_text='<br/>'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='film',
            name='personnages',
            field=models.ManyToManyField(verbose_name='Personnages', through='siteweb.Incarnerpersonnage', to='siteweb.Personnage', blank=True, help_text='<br/>'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='film',
            name='realisateurs',
            field=models.ManyToManyField(verbose_name='Réalisateurs', to='siteweb.Artiste', blank=True, help_text='<br/>', related_name='realisateurs'),
            preserve_default=True,
        ),
    ]
