# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('siteweb', '0002_auto_20150304_0305'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='artiste',
            options={'ordering': ['nom']},
        ),
        migrations.AlterModelOptions(
            name='film',
            options={'ordering': ['titre']},
        ),
        migrations.AlterModelOptions(
            name='genre',
            options={'ordering': ['nom']},
        ),
        migrations.AlterModelOptions(
            name='nationalite',
            options={'ordering': ['nom']},
        ),
        migrations.AlterModelOptions(
            name='personnage',
            options={'ordering': ['nom']},
        ),
    ]
