# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('siteweb', '0004_auto_20150304_0446'),
    ]

    operations = [
        migrations.AlterField(
            model_name='film',
            name='forum',
            field=models.OneToOneField(null=True, help_text='<br/>', to='siteweb.Forum', verbose_name='Forum'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='groupe',
            name='forum',
            field=models.OneToOneField(null=True, help_text='<br/>', to='siteweb.Forum', verbose_name='Forum'),
            preserve_default=True,
        ),
    ]
