# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('siteweb', '0012_auto_20150304_0522'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artiste',
            name='nationalites',
            field=models.ManyToManyField(help_text=b'<br/>', to='siteweb.Nationalite', verbose_name=b'Nationalit\xc3\xa9s', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='artiste',
            name='prenom',
            field=models.CharField(max_length=100, verbose_name=b'Pr\xc3\xa9nom', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='film',
            name='nationalites',
            field=models.ManyToManyField(help_text=b'<br/>', to='siteweb.Nationalite', verbose_name=b'Nationalit\xc3\xa9s', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='film',
            name='realisateurs',
            field=models.ManyToManyField(help_text=b'<br/>', related_name='realisateurs', verbose_name=b'R\xc3\xa9alisateurs', to='siteweb.Artiste', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='forum',
            name='moderateur',
            field=models.ForeignKey(verbose_name=b'Mod\xc3\xa9rateur', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='genre',
            name='nom',
            field=models.CharField(max_length=100, verbose_name=b'Intitul\xc3\xa9'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='groupe',
            name='nationalites',
            field=models.ManyToManyField(help_text=b'<br/>', to='siteweb.Nationalite', verbose_name=b'Nationalit\xc3\xa9s', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='dateenvoi',
            field=models.DateTimeField(default=datetime.datetime.now),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='nationalite',
            name='nom',
            field=models.CharField(max_length=100, verbose_name=b'Intitul\xc3\xa9'),
            preserve_default=True,
        ),
    ]
