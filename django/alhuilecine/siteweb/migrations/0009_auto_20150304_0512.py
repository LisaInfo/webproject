# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('siteweb', '0008_auto_20150304_0450'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='dateenvoi',
            field=models.DateTimeField(),
            preserve_default=True,
        ),
    ]
