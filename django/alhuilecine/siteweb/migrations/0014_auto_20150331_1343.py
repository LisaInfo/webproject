# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('siteweb', '0013_auto_20150310_1520'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artiste',
            name='nationalites',
            field=models.ManyToManyField(help_text=b'<br/>', to='siteweb.Nationalite', verbose_name='Nationalit\xe9s', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='artiste',
            name='prenom',
            field=models.CharField(max_length=100, verbose_name='Pr\xe9nom', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='film',
            name='nationalites',
            field=models.ManyToManyField(help_text=b'<br/>', to='siteweb.Nationalite', verbose_name='Nationalit\xe9s', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='film',
            name='realisateurs',
            field=models.ManyToManyField(help_text=b'<br/>', related_name='realisateurs', verbose_name='R\xe9alisateurs', to='siteweb.Artiste', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='forum',
            name='moderateur',
            field=models.ForeignKey(verbose_name='Mod\xe9rateur', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='genre',
            name='nom',
            field=models.CharField(max_length=100, verbose_name='Intitul\xe9'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='groupe',
            name='nationalites',
            field=models.ManyToManyField(help_text=b'<br/>', to='siteweb.Nationalite', verbose_name='Nationalit\xe9s', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='nationalite',
            name='nom',
            field=models.CharField(max_length=100, verbose_name='Intitul\xe9'),
            preserve_default=True,
        ),
    ]
