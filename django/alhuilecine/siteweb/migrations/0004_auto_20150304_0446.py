# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('siteweb', '0003_auto_20150304_0330'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='avoirpourreponse',
            name='messagefils',
        ),
        migrations.RemoveField(
            model_name='avoirpourreponse',
            name='messagepere',
        ),
        migrations.DeleteModel(
            name='Avoirpourreponse',
        ),
        migrations.RemoveField(
            model_name='forumconcernerfilm',
            name='film',
        ),
        migrations.RemoveField(
            model_name='forumconcernerfilm',
            name='forum',
        ),
        migrations.DeleteModel(
            name='Forumconcernerfilm',
        ),
        migrations.RemoveField(
            model_name='forumconcernergroupe',
            name='forum',
        ),
        migrations.RemoveField(
            model_name='forumconcernergroupe',
            name='groupe',
        ),
        migrations.DeleteModel(
            name='Forumconcernergroupe',
        ),
        migrations.RemoveField(
            model_name='groupeconcernerartiste',
            name='artiste',
        ),
        migrations.RemoveField(
            model_name='groupeconcernerartiste',
            name='groupe',
        ),
        migrations.DeleteModel(
            name='Groupeconcernerartiste',
        ),
        migrations.RemoveField(
            model_name='groupeconcernerfilm',
            name='film',
        ),
        migrations.RemoveField(
            model_name='groupeconcernerfilm',
            name='groupe',
        ),
        migrations.DeleteModel(
            name='Groupeconcernerfilm',
        ),
        migrations.RemoveField(
            model_name='groupeconcernergenre',
            name='genre',
        ),
        migrations.RemoveField(
            model_name='groupeconcernergenre',
            name='groupe',
        ),
        migrations.DeleteModel(
            name='Groupeconcernergenre',
        ),
        migrations.RemoveField(
            model_name='groupeconcernernationalite',
            name='groupe',
        ),
        migrations.RemoveField(
            model_name='groupeconcernernationalite',
            name='nationalite',
        ),
        migrations.DeleteModel(
            name='Groupeconcernernationalite',
        ),
        migrations.RemoveField(
            model_name='groupeconcernerpersonnage',
            name='groupe',
        ),
        migrations.RemoveField(
            model_name='groupeconcernerpersonnage',
            name='personnage',
        ),
        migrations.DeleteModel(
            name='Groupeconcernerpersonnage',
        ),
        migrations.AddField(
            model_name='film',
            name='forum',
            field=models.OneToOneField(help_text='<br/>', verbose_name='Forum', to='siteweb.Forum', default=None, null=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='groupe',
            name='artistes',
            field=models.ManyToManyField(to='siteweb.Artiste', help_text='<br/>', blank=True, verbose_name='Artistes'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='groupe',
            name='films',
            field=models.ManyToManyField(to='siteweb.Film', help_text='<br/>', blank=True, verbose_name='Films'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='groupe',
            name='forum',
            field=models.OneToOneField(help_text='<br/>', verbose_name='Forum', to='siteweb.Forum', default=None, null=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='groupe',
            name='genres',
            field=models.ManyToManyField(to='siteweb.Genre', help_text='<br/>', blank=True, verbose_name='Genres'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='groupe',
            name='nationalites',
            field=models.ManyToManyField(to='siteweb.Nationalite', help_text='<br/>', blank=True, verbose_name='Nationalités'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='groupe',
            name='personnages',
            field=models.ManyToManyField(to='siteweb.Personnage', help_text='<br/>', blank=True, verbose_name='Personnages'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='message',
            name='enreponsea',
            field=models.ForeignKey(null=True, to='siteweb.Message'),
            preserve_default=True,
        ),
    ]
