-- Drop des tables
drop table ChangementStatut;
drop table Noter;
drop table Note;
drop table ArtisteAvoirNationalite;
drop table FilmAvoirNationalite;
drop table AvoirGenre;
drop table EtreRealisateur;
drop table GroupeConcernerFilm;
drop table GroupeConcernerNationalite;
drop table GroupeConcernerGenre;
drop table GroupeConcernerArtiste;
drop table GroupeConcernerPersonnage;
drop table SondageConcernerGroupe;
drop table IncarnerPersonnage;
drop table EtreActeur;
drop table EtrePersonnage;
drop table Personnage;
drop table Artiste;
drop table ForumConcernerFilm;
drop table ForumConcernerGroupe;
drop table Film;
drop table Nationalite;
drop table Genre;
drop table AvoirPourReponse;
drop table Message;
drop table Forum;
drop table Groupe;
drop table VoteSondage;
drop table OptionSondage;
drop table Sondage;
drop table Utilisateur;
drop table Statut;

-- Table Genre
create table Genre(
  id int not null, 
  nom varchar(100) not null,
  primary key(id)
);

-- Table Nationalite
create table Nationalite(
  id int not null, 
  nom varchar(100) not null,
  primary key(id)
);

-- Table Film
create table Film(
  id int not null, 
  titre varchar(100) not null,
  dateSortie date not null,
  synopsis varchar(1000),
  primary key(id)
);

-- Table Artiste
create table Artiste(
  id int not null, 
  nom varchar(100) not null,
  prenom varchar(100),
  dateNaissance date,
  dateMort date,
  biographie varchar(1000),
  primary key(id)
);

-- Table Personnage
create table Personnage(
  id int not null,
  nom varchar(100) not null,
  primary key(id)
);

-- Table EtrePersonnage
create table EtrePersonnage(
  idPersonnage int not null, 
  idFilm int not null,
  primary key(idPersonnage, idFilm),
  foreign key(idPersonnage) references Personnage(id),
  foreign key(idFilm) references Film(id)
);

-- Table EtreActeur
create table EtreActeur(
  idArtiste int not null, 
  idFilm int not null,
  primary key(idArtiste, idFilm),
  foreign key(idArtiste) references Artiste(id),
  foreign key(idFilm) references Film(id)
);

-- Table IncarnerPersonnage
create table IncarnerPersonnage(
  idArtiste int not null, 
  idPersonnage int not null,
  idFilm int not null,
  primary key(idArtiste, idPersonnage, idFilm),
  foreign key(idArtiste, idFilm) references EtreActeur(idArtiste, idFilm),
  foreign key(idPersonnage, idFilm) references EtrePersonnage(idPersonnage, idFilm)
);

-- Table EtreRealisateur
create table EtreRealisateur(
  idArtiste int not null, 
  idFilm int not null,
  primary key(idArtiste, idFilm),
  foreign key(idArtiste) references Artiste(id),
  foreign key(idFilm) references Film(id)
);

-- Table AvoirGenre
create table AvoirGenre(
  idFilm int not null, 
  idGenre int not null,
  primary key(idFilm, idGenre),
  foreign key(idFilm) references Film(id),
  foreign key(idGenre) references Genre(id)
);

-- Table FilmAvoirNationalite
create table FilmAvoirNationalite(
  idFilm int not null, 
  idNationalite int not null,
  primary key(idFilm, idNationalite),
  foreign key(idFilm) references Film(id),
  foreign key(idNationalite) references Nationalite(id)
);

-- Table ArtisteAvoirNationalite
create table ArtisteAvoirNationalite(
  idArtiste int not null, 
  idNationalite int not null,
  primary key(idArtiste, idNationalite),
  foreign key(idArtiste) references Artiste(id),
  foreign key(idNationalite) references Nationalite(id)
);

-- Table Statut
create table Statut(
  id int not null, 
  nom varchar(100) not null,
  primary key(id)
);

-- Table Utilisateur
create table Utilisateur(
  pseudo varchar(20) not null, 
  motDePasse varchar(20) not null,
  idStatut int not null,
  primary key(pseudo),
  foreign key(idStatut) references Statut(id)
);

-- Table Note
create table Note(
  id int not null, 
  valeur int not null check(valeur between 0 and 5),
  primary key(id)
);

-- Table Noter
create table Noter(
  pseudoUtilisateur varchar(20) not null, 
  idFilm int not null,
  idNote int not null,
  primary key(pseudoUtilisateur, idFilm),
  foreign key(pseudoUtilisateur) references Utilisateur(pseudo),
  foreign key(idFilm) references Film(id),
  foreign key(idNote) references Note(id)
);

-- Table ChangementStatut
create table ChangementStatut(
  id int not null,
  pseudoUtilisateur varchar(20) not null,
  dateChangement date not null,
  idNouveauStatut int not null,
  idAncienStatut int not null,
  pseudoAdministrateur varchar(20),
  primary key(id),
  foreign key(pseudoAdministrateur) references Utilisateur(pseudo)
);

-- Table Groupe
create table Groupe(
  id int not null, 
  nom varchar(100) not null,
  prive bool not null,
  primary key(id)
);

-- Table GroupeConcernerFilm
create table GroupeConcernerFilm(
  idGroupe int not null, 
  idFilm int not null,
  primary key(idGroupe, idFilm),
  foreign key(idGroupe) references Groupe(id),
  foreign key(idFilm) references Film(id)
);

-- Table GroupeConcernerNationalite
create table GroupeConcernerNationalite(
  idGroupe int not null, 
  idNationalite int not null,
  primary key(idGroupe, idNationalite),
  foreign key(idGroupe) references Groupe(id),
  foreign key(idNationalite) references Nationalite(id)
);

-- Table GroupeConcernerGenre
create table GroupeConcernerGenre(
  idGroupe int not null, 
  idGenre int not null,
  primary key(idGroupe, idGenre),
  foreign key(idGroupe) references Groupe(id),
  foreign key(idGenre) references Genre(id)
);

-- Table GroupeConcernerArtiste
create table GroupeConcernerArtiste(
  idGroupe int not null, 
  idArtiste int not null,
  primary key(idGroupe, idArtiste),
  foreign key(idGroupe) references Groupe(id),
  foreign key(idArtiste) references Artiste(id)
);

-- Table GroupeConcernerPersonnage
create table GroupeConcernerPersonnage(
  idGroupe int not null, 
  idPersonnage int not null,
  primary key(idGroupe, idPersonnage),
  foreign key(idGroupe) references Groupe(id),
  foreign key(idPersonnage) references Personnage(id)
);

-- Table Forum
create table Forum(
  id int not null,
  pseudoModerateur varchar(20) not null,
  primary key(id),
  foreign key(pseudoModerateur) references Utilisateur(pseudo)
);

-- Table Message
create table Message(
  id int not null,
  texte varchar(3000) not null,
  dateEnvoi date not null,
  idForum int not null,
  pseudoEnvoyeur varchar(20) not null,
  primary key(id),
  foreign key(idForum) references Forum(id),
  foreign key(pseudoEnvoyeur) references Utilisateur(pseudo)
);

-- Table AvoirPourReponse
create table AvoirPourReponse(
  idMessagePere int not null,
  idMessageFils int not null,
  primary key(idMessagePere, idMessageFils),
  foreign key(idMessagePere) references Message(id),
  foreign key(idMessageFils) references Message(id)
);

-- Table Sondage
create table Sondage(
  id int not null,
  question varchar(1000) not null,
  dateProposition date not null,
  pseudoProposeur varchar(20) not null,
  primary key(id),
  foreign key(pseudoProposeur) references Utilisateur(pseudo)
);

-- Table SondageConcernerGroupe
create table SondageConcernerGroupe(
  idSondage int not null,
  idGroupe int not null, 
  primary key(idSondage, idGroupe),
  foreign key(idSondage) references Sondage(id),
  foreign key(idGroupe) references Groupe(id)
);

-- Table OptionSondage
create table OptionSondage(
  id int not null,
  idSondage int not null,
  texte varchar(1000) not null,
  primary key(id),
  foreign key(idSondage) references Sondage(id)
);

-- Table VoteSondage
create table VoteSondage(
  pseudoVoteur varchar(20) not null,
  idSondage int not null,
  idOptionSondage int not null,
  primary key(pseudoVoteur, idSondage),
  foreign key(pseudoVoteur) references Utilisateur(pseudo),
  foreign key(idSondage) references Sondage(id),
  foreign key(idOptionSondage) references OptionSondage(id)
);

-- Table ForumConcernerFilm
create table ForumConcernerFilm(
  idForum int not null,
  idFilm int not null,
  primary key(idForum, idFilm),
  foreign key(idForum) references Forum(id),
  foreign key(idFilm) references Film(id)
);

-- Table ForumConcernerGroupe
create table ForumConcernerGroupe(
  idForum int not null,
  idGroupe int not null,
  primary key(idForum, idGroupe),
  foreign key(idForum) references Forum(id),
  foreign key(idGroupe) references Groupe(id)
);
