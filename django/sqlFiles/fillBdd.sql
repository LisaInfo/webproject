-- Insert Genre
insert into Genre(nom) values('Action');
insert into Genre(nom) values('Comédie');
insert into Genre(nom) values('Horreur');
insert into Genre(nom) values('Policier');

-- Insert Nationalite
insert into Nationalite(nom) values('Espagnol');
insert into Nationalite(nom) values('Français');
insert into Nationalite(nom) values('Vietnamien');

-- Insert Film
insert into Film(titre, dateSortie, synopsis)
values(
  'Subway',
  '1985-04-10',
  'Après avoir dérobé des documents compromettants, un homme se réfugie dans l\'univers fascinant et agité du métro parisien. Une impitoyable chasse à l\'homme s\'organise. D\'étranges liens se tissent entre le cambrioleur, Fred, et la femme de sa victime, Helena.

Au-delà de l\'histoire de vol de documents et de chantage, Subway est aussi une plongée dans le monde de la marginalité. Fred parcourt les sous-sols du métro et découvre sa faune nocturne. Au détour des couloirs, il rencontre des musiciens, chacun dans leur coin. Il décide de les réunir et de les inciter à former un groupe.'
);

-- Insert FilmAvoirNationalite
insert into FilmAvoirNationalite(idFilm, idNationalite)
values( 
  (select id from Film where titre = 'Subway'),
  (select id from Nationalite where nom = 'Français')
);

-- Insert AvoirGenre
insert into AvoirGenre(idFilm, idGenre)
values( 
  (select id from Film where titre = 'Subway'),
  (select id from Genre where nom = 'Policier')
);

-- Insert Artiste
insert into Artiste(nom, prenom, dateNaissance, biographie)
values(
  'Besson',
  'Luc',
  '1959-03-18',
  'Fils unique, Luc Besson passe son enfance auprès de parents instructeurs en plongée sous-marine au Club Méditerranée, entre la Grèce et l\'ex-Yougoslavie. Un accident l\'empêche de poursuivre la plongée à haut niveau.'
);
insert into Artiste(nom, prenom, dateNaissance, biographie)
values(
  'Lambert',
  'Christophe',
  '1957-03-27',
  'Né à Great Neck, en banlieue de New York, il passe son enfance en Suisse et en France à Annemasse où il étudie au lycée, où son père officie en tant que diplomate à l\'ONU. Après une année passée au Conservatoire de Paris, il débute sur grand écran en 1980 dans le thriller Le Bar du téléphone. Il est révélé au grand public quatre ans plus tard en incarnant le personnage de Tarzan dans Greystoke, la légende de Tarzan, qui remporte un succès mondial.'
);

-- Insert ArtisteAvoirNationalite
insert into ArtisteAvoirNationalite(idArtiste, idNationalite)
values(
 (select id from Artiste where nom = 'Besson' and prenom = 'Luc'),
  (select id from Nationalite where nom = 'Français')
);
insert into ArtisteAvoirNationalite(idArtiste, idNationalite)
values(
 (select id from Artiste where nom = 'Lambert' and prenom = 'Christophe'),
  (select id from Nationalite where nom = 'Français')
);

-- Insert EtreRealisateur
insert into EtreRealisateur(idArtiste, idFilm)
values(
  (select id from Artiste where nom = 'Besson' and prenom = 'Luc'),
  (select id from Film where titre = 'Subway')
);

-- Insert Personnage
insert into Personnage(nom) values('Fred');

-- Insert EtrePersonnage
insert into EtrePersonnage(idPersonnage, idFilm)
values(
  (select id from Personnage where nom = 'Fred'),
  (select id from Film where titre = 'Subway')
);

-- Insert EtreActeur
insert into EtreActeur(idArtiste, idFilm)
values(
  (select id from Artiste where nom = 'Lambert' and prenom = 'Christophe'),
  (select id from Film where titre = 'Subway')
);

-- Insert IncarnerPersonnage
insert into IncarnerPersonnage(idArtiste, idPersonnage, idFilm)
values(
  (select idArtiste from EtreActeur
   where idFilm = (select id from Film where titre = 'Subway')
   and   idArtiste = (select id from Artiste where nom = 'Lambert' and prenom = 'Christophe')
  ),
  (select idPersonnage from EtrePersonnage
   where idFilm = (select id from Film where titre = 'Subway')
   and   idPersonnage = (select id from Personnage where nom = 'Fred')
  ),
  (select id from Film where titre = 'Subway')
);

-- Insert Statut
insert into Statut(nom) values('Administrateur');
insert into Statut(nom) values('Utilisateur');

-- Insert Utilisateur
insert into Utilisateur(pseudo, motDePasse, idStatut)
values(
  'aniwey',
  'lapin',
  (select id from Statut where nom = 'Administrateur')
);
insert into Utilisateur(pseudo, motDePasse, idStatut)
values(
  'lisa',
  'aze',
  (select id from Statut where nom = 'Administrateur')
);
insert into Utilisateur(pseudo, motDePasse, idStatut)
values(
  'jeremy',
  'jeremy',
  (select id from Statut where nom = 'Utilisateur')
);

-- Insert Note
insert into Note(valeur) values(0);
insert into Note(valeur) values(1);
insert into Note(valeur) values(2);
insert into Note(valeur) values(3);
insert into Note(valeur) values(4);
insert into Note(valeur) values(5);

-- Insert Noter
insert into Noter(pseudoUtilisateur, idFilm, idNote)
values(
  'lisa',
  (select id from Film where titre = 'Subway'),
  (select id from Note where valeur = 4)
);

-- Insert ChangementStatut
insert into ChangementStatut(pseudoUtilisateur, dateChangement, idNouveauStatut, idAncienStatut, pseudoAdministrateur)
values(
  'aniwey',
  '2014-05-15',
  (select id from Statut where nom = 'Administrateur'),
  (select id from Statut where nom = 'Utilisateur'),
  'lisa'
);

-- Insert Groupe
insert into Groupe(nom, prive)
values(
  'Pour tous ceux qui aiment le métro parisien',
  FALSE
);

-- Insert GroupeConcernerFilm
insert into GroupeConcernerFilm(idGroupe, idFilm)
values(
  (select id from Groupe where nom = 'Pour tous ceux qui aiment le métro parisien'),
  (select id from Film where titre = 'Subway')
);

-- Insert GroupeConcernerNationalite
insert into GroupeConcernerNationalite(idGroupe, idNationalite)
values(
  (select id from Groupe where nom = 'Pour tous ceux qui aiment le métro parisien'),
  (select id from Nationalite where nom = 'Français')
);

-- Insert GroupeConcernerGenre
insert into GroupeConcernerGenre(idGroupe, idGenre)
values(
  (select id from Groupe where nom = 'Pour tous ceux qui aiment le métro parisien'),
  (select id from Genre where nom = 'Policier')
);

-- Insert GroupeConcernerArtiste
insert into GroupeConcernerArtiste(idGroupe, idArtiste)
values(
  (select id from Groupe where nom = 'Pour tous ceux qui aiment le métro parisien'),
  (select id from Artiste where nom = 'Lambert' and prenom = 'Christophe')
);



